#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>

#define READ 0
#define WRITE 1
#define OUT_FILE_P3 "process-3.txt"


typedef struct {
    char *string;
    int size;
    int top;
} String;


void initializeString(String *string, int initialSize);

void sendToMain(int readPipeProcess2, int *pipefd);

int *convertIntArray(char *str);

String convertCharArray(int *array);

unsigned char get_subbox_val(unsigned char val);

void concatString(String *string, char *newCharArray);


int main(int argc, char **argv) {

    int pipefd[2];
    pipe(pipefd);

    pid_t pid = fork();
    if (pid == 0) {
        String readPipe;
        initializeString(&readPipe, 100);
        sprintf(readPipe.string, "%d", pipefd[READ]);
        /* call main process for the last time */
        execl("./main", readPipe.string, NULL);
        printf("Process-3 exec failed.\n");
    } else {
        int readPipeProcess1 = atoi(argv[0]);
        sendToMain(readPipeProcess1, pipefd); /* send cipher text to main process */
        close(readPipeProcess1);
        exit(EXIT_SUCCESS);
    }
    return EXIT_SUCCESS;
}

void sendToMain(int readPipeProcess2, int *pipefd) {
    String buffer;
    FILE *file;
    int i;

    initializeString(&buffer, 100);
    file = fopen(OUT_FILE_P3, "w");


    /* read permutated strings */
    while (read(readPipeProcess2, buffer.string, 100) != 0) {
        int *cipher = convertIntArray(buffer.string);
        /* apply subbox process */
        for (i = 0; i < 16; i++) {
            cipher[i] = get_subbox_val((unsigned char) cipher[i]);
        }
        String cipherText = convertCharArray(cipher);
        /* flush the file */
        fprintf(file, "%s\n", cipherText.string);
        fflush(file);
        /* send cipher to main */
        write(pipefd[WRITE], cipherText.string, (size_t) cipherText.size);
    }
    fclose(file);
    close(pipefd[WRITE]);
}


int *convertIntArray(char *str) {
    char *token;
    int *array;
    int i;

    array = (int *) malloc(16 * sizeof(int));

    i = 0;
    token = strtok(str, "-");
    while (token != NULL) {
        array[i] = atoi(token);
        i++;
        token = strtok(NULL, "-");
    }
    return array;
}

String convertCharArray(int *array) {
    String temp;
    int i;
    initializeString(&temp, 100);
    for (i = 0; i < 16; i++) {
        char one[10];
        sprintf(one, "%d", array[i]);
        concatString(&temp, one);
        if (i != 15) {
            concatString(&temp, "-\0");
        }
    }
    return temp;
}

unsigned char get_subbox_val(unsigned char val) {
    unsigned char SUBBOX[256] = {47, 164, 147, 166, 221, 246, 1, 13, 198, 78, 102, 219, 75, 97, 62, 140,
                                 84, 69, 107, 99, 185, 220, 179, 61, 187, 0, 92, 112, 8, 33, 15, 119,
                                 209, 178, 192, 12, 121, 239, 117, 96, 100, 126, 118, 199, 208, 50, 42, 168,
                                 14, 171, 17, 238, 158, 207, 144, 58, 127, 182, 146, 71, 68, 157, 154, 88,
                                 248, 105, 131, 235, 98, 170, 22, 160, 181, 4, 254, 70, 202, 225, 67, 205,
                                 216, 25, 43, 222, 236, 128, 122, 77, 59, 145, 167, 54, 20, 55, 152, 149,
                                 230, 211, 224, 111, 165, 124, 16, 243, 213, 114, 116, 63, 64, 176, 31, 161,
                                 9, 229, 95, 247, 193, 18, 134, 79, 133, 173, 82, 51, 57, 136, 6, 49,
                                 5, 197, 115, 65, 169, 255, 249, 195, 30, 162, 150, 53, 83, 46, 228, 81,
                                 237, 104, 28, 223, 217, 251, 200, 60, 132, 194, 151, 137, 191, 74, 201, 103,
                                 29, 80, 113, 101, 250, 172, 234, 180, 73, 141, 204, 27, 241, 188, 153, 155,
                                 86, 94, 177, 87, 39, 91, 2, 48, 35, 40, 120, 159, 184, 123, 215, 138,
                                 210, 108, 76, 106, 36, 189, 125, 226, 252, 37, 66, 156, 253, 218, 85, 203,
                                 110, 10, 244, 45, 34, 242, 72, 93, 52, 135, 44, 245, 3, 32, 196, 163,
                                 232, 240, 227, 24, 139, 183, 38, 233, 130, 143, 109, 41, 174, 231, 129, 23,
                                 148, 89, 212, 19, 21, 142, 7, 214, 56, 90, 11, 190, 175, 206, 26, 186};
    unsigned char sub_box_val = SUBBOX[val];
    return sub_box_val;
}

/**
 * String initializer
 * @param string String structure
 * @param initialSize size must be positive
 */
void initializeString(String *string, int initialSize) {
    string->string = (char *) malloc(initialSize * sizeof(char));
    string->string[0] = '\0';
    string->size = initialSize;
    string->top = -1;
}


/**
 * concats strings (size auto increased)
 * @param string
 * @param newCharArray
 */
void concatString(String *string, char *newCharArray) {

    /* concats two strings */

    int i;
    for (i = 0; newCharArray[i] != '\0'; i++) {
        if (string->top == string->size - 1) {
            string->size *= 2;
            string->string = (char *) realloc(string->string, string->size * sizeof(char));
        }
        string->top++;
        string->string[string->top] = newCharArray[i];
    }

    if (string->top == string->size - 1) {
        string->size *= 2;
        string->string = (char *) realloc(string->string, string->size * sizeof(char));
    }

    string->string[string->top + 1] = '\0';
}