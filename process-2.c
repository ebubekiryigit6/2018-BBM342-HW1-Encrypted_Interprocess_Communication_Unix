#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>

#define READ 0
#define WRITE 1
#define OUT_FILE_P2 "process-2.txt"


typedef struct {
    char *string;
    int size;
    int top;
} String;


int *convertIntArray(char *str);

void initializeString(String *string, int initialSize);

void sendToProcess3(int readPipeMain, int *pipefd);

void concatString(String *string, char *newCharArray);

void swap(int *a, int *b);

String convertCharArray(int *array);

int main(int argc, char **argv) {

    int pipefd[2];
    pipe(pipefd);


    pid_t pid = fork();
    if (pid == 0) {
        String readPipe;
        initializeString(&readPipe, 100);
        sprintf(readPipe.string, "%d", pipefd[READ]);
        /* run process-3 */
        execl("./process-3", readPipe.string, NULL);
        printf("Process-2 exec failed.\n");
    } else {
        int readPipeProcess1 = atoi(argv[0]);
        sendToProcess3(readPipeProcess1, pipefd); /* send permutation string */
        close(readPipeProcess1);
        exit(EXIT_SUCCESS);
    }
    return EXIT_SUCCESS;
}

void sendToProcess3(int readPipeMain, int *pipefd) {
    String buffer;
    FILE *file;
    int i;

    initializeString(&buffer, 100);
    file = fopen(OUT_FILE_P2, "w");

    /* read all of xor strings and apply permutation */
    while (read(readPipeMain, buffer.string, 100) != 0) {
        int *cipher = convertIntArray(buffer.string);
        /* permutation swap */
        for (i = 0; i < 8; i++) {
            swap(&cipher[i], &cipher[i + 8]);
        }
        String cipherText = convertCharArray(cipher);
        /* write to file */
        fprintf(file, "%s\n", cipherText.string);
        fflush(file);
        /* send to process 3 */
        write(pipefd[WRITE], cipherText.string, (size_t) cipherText.size);
    }
    fclose(file);
    close(pipefd[WRITE]);
}

/* basic array swap */
void swap(int *a, int *b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}


int *convertIntArray(char *str) {
    char *token;
    int *array;
    int i;

    array = (int *) malloc(16 * sizeof(int));

    i = 0;
    token = strtok(str, "-");
    while (token != NULL) {
        array[i] = atoi(token);
        i++;
        token = strtok(NULL, "-");
    }
    return array;
}

String convertCharArray(int *array) {
    String temp;
    int i;
    initializeString(&temp, 100);
    for (i = 0; i < 16; i++) {
        char one[10];
        sprintf(one, "%d", array[i]);
        concatString(&temp, one);
        if (i != 15) {
            concatString(&temp, "-\0");
        }
    }
    return temp;
}


/**
 * String initializer
 * @param string String structure
 * @param initialSize size must be positive
 */
void initializeString(String *string, int initialSize) {
    string->string = (char *) malloc(initialSize * sizeof(char));
    string->string[0] = '\0';
    string->size = initialSize;
    string->top = -1;
}

/**
 * concats strings (size auto increased)
 * @param string
 * @param newCharArray
 */
void concatString(String *string, char *newCharArray) {

    /* concats two strings */

    int i;
    for (i = 0; newCharArray[i] != '\0'; i++) {
        if (string->top == string->size - 1) {
            string->size *= 2;
            string->string = (char *) realloc(string->string, string->size * sizeof(char));
        }
        string->top++;
        string->string[string->top] = newCharArray[i];
    }

    if (string->top == string->size - 1) {
        string->size *= 2;
        string->string = (char *) realloc(string->string, string->size * sizeof(char));
    }

    string->string[string->top + 1] = '\0';
}