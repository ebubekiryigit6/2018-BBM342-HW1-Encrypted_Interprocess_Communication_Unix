#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>

#define READ 0
#define WRITE 1
#define OUT_FILE_P1 "process-1.txt"

typedef struct {
    char *string;
    int size;
    int top;
} String;


void initializeString(String *string, int initialSize);

void copyString(String *string, char *newCharArray);

void clearString(String *string);

void sendToProcess2(int readPipeMain, int *pipefd);

int *convertIntArray(char *str);

void concatString(String *string, char *newCharArray);

int xor(int x, int y);

String convertCharArray(int *array);

int main(int argc, char **argv) {

    int pipefd[2];
    pipe(pipefd);


    pid_t pid = fork();
    if (pid == 0) {
        String readPipe;
        initializeString(&readPipe, 100);
        sprintf(readPipe.string, "%d", pipefd[READ]);
        /* run process-2 */
        execl("./process-2", readPipe.string, NULL);
        printf("Process-1 exec failed.\n");
    } else {
        /* get pipe that comes from main */
        int readPipeMain = atoi(argv[0]);
        sendToProcess2(readPipeMain, pipefd); /* send xor text */
        close(readPipeMain);
        exit(EXIT_SUCCESS);
    }
    return EXIT_SUCCESS;
}

void sendToProcess2(int readPipeMain, int *pipefd) {
    int key_flag;

    String buffer;
    String key;
    int *keyArray;
    FILE *file;
    int i;

    initializeString(&buffer, 100);
    initializeString(&key, 100);
    file = fopen(OUT_FILE_P1, "w");

    key_flag = 0;
    /* read all of plain texts and keys that comes from main process */
    while (read(readPipeMain, buffer.string, 100) != 0) {
        if (key_flag == 0) {
            copyString(&key, buffer.string);
            keyArray = convertIntArray(key.string);
            /* got key */
        } else {
            int *cipher = (int *) malloc(16 * sizeof(int));
            int *plainArray = convertIntArray(buffer.string);

            /* apply xor operation */

            for (i = 0; i < 16; i++) {
                cipher[i] = xor(keyArray[i], plainArray[i]);
            }
            String cipherText = convertCharArray(cipher);
            fprintf(file, "%s\n", cipherText.string);
            fflush(file);
            /* send it to process2 */
            write(pipefd[WRITE], cipherText.string, (size_t) cipherText.size);
        }
        key_flag++;
        clearString(&buffer);
    }
    close(pipefd[WRITE]);
    fclose(file);
}

/*bitwise xor operation*/
int xor(int x, int y) {
    int a = x & y;
    int b = ~x & ~y;
    int z = ~a & ~b;
    return z;
}

/**
 * array converter splits
 * string with - and adds to int array
 *
 * @param str
 * @return
 */
int *convertIntArray(char *str) {
    char *token;
    int *array;
    int i;

    array = (int *) malloc(16 * sizeof(int));

    i = 0;
    token = strtok(str, "-");
    while (token != NULL) {
        array[i] = atoi(token);
        i++;
        token = strtok(NULL, "-");
    }
    return array;
}

/**
 * formats int array to string with delimiter -
 *
 * @param array
 * @return
 */
String convertCharArray(int *array) {
    String temp;
    int i;
    initializeString(&temp, 100);
    for (i = 0; i < 16; i++) {
        char one[10];
        sprintf(one, "%d", array[i]);
        concatString(&temp, one);
        if (i != 15) {
            concatString(&temp, "-\0");
        }
    }
    return temp;
}


/**
 * String initializer
 * @param string String structure
 * @param initialSize size must be positive
 */
void initializeString(String *string, int initialSize) {
    string->string = (char *) malloc(initialSize * sizeof(char));
    string->string[0] = '\0';
    string->size = initialSize;
    string->top = -1;
}

/**
 * copies a string (auto increased size)
 * @param string
 * @param newCharArray
 */
void copyString(String *string, char *newCharArray) {

    /* copies a char* begining of the string */

    int i;
    for (i = 0; newCharArray[i] != '\0'; i++) {
        if (string->top == string->size - 1) {
            string->size *= 2;
            string->string = (char *) realloc(string->string, string->size * sizeof(char));
        }
        string->top++;
        string->string[i] = newCharArray[i];
    }
    if (string->top == string->size - 1) {
        string->size *= 2;
        string->string = (char *) realloc(string->string, string->size * sizeof(char));
    }
    string->string[i] = '\0';
}

/**
 * concats strings (size auto increased)
 * @param string
 * @param newCharArray
 */
void concatString(String *string, char *newCharArray) {

    /* concats two strings */

    int i;
    for (i = 0; newCharArray[i] != '\0'; i++) {
        if (string->top == string->size - 1) {
            string->size *= 2;
            string->string = (char *) realloc(string->string, string->size * sizeof(char));
        }
        string->top++;
        string->string[string->top] = newCharArray[i];
    }

    if (string->top == string->size - 1) {
        string->size *= 2;
        string->string = (char *) realloc(string->string, string->size * sizeof(char));
    }

    string->string[string->top + 1] = '\0';
}

/**
 * Clear all chars in string but size is same
 * @param string
 */
void clearString(String *string) {

    /* string's chars NULL char "reset string" */

    int i;
    for (i = 0; i <= string->top; i++) {
        string->string[i] = '\0';
    }

    string->top = -1;
}