/**
 *
 * Author:      Ebubekir Yigit
 *
 * ID:          21328629
 *
 * File:        main.c
 *
 * Purpose:     The purpose of the assignment; to develop a multiprocessing cipher that communicates with pipes.
 *              Each process will create its own child and the last child will go back to the main process.
 *              Each process will write the operations it has done to the relevant file.
 *              Each process will serve in separate ".c" files.
 *
 *
 *
 * Compile:     gcc main.c -o main -ansi -Wall
 *              gcc process-1.c -o process-1 -ansi -Wall
 *              gcc process-2.c -o process-2 -ansi -Wall
 *              gcc process-3.c -o process-3 -ansi -Wall
 *
 * Run:         ./main ./sample-1/plain.txt ./sample-1/key.txt
 *
 *
 * Input:       None
 *
 * Output:      Please See -->  ./process-1.txt     ./process-2.txt     ./process-3.txt     ./cipher.txt
 *
 *
 *
 * Notes:       I copied the same functions as the header files and different ".c" files do not fit the submit format.
 *              Sorry for that.
 *
 *              And also, Code has been tested with all of the shared sample files.
 *              All tests were successful, I hope code succeed when you try :) Good work.
 *
 *
 */


#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>

#define READ 0
#define WRITE 1
#define OUT_FILE_MAIN "./cipher.txt"

typedef struct {
    char *string;
    int size;
    int top;
} String;


void usage();

String getKey(char *keyFilePath);

void initializeString(String *string, int initialSize);

void concatString(String *string, char *newCharArray);

void clearString(String *string);

void sendToProcess1(char *plainTextPath, int *pipefd, String key);

void finishProgram(int readPipeProcess3);


int main(int argc, char **argv) {

    if (argc != 3) {
        if (argc == 1) {
            /* called from process 3 */
            int readPipeProcess1 = atoi(argv[0]);
            finishProgram(readPipeProcess1);
            close(readPipeProcess1);
            exit(EXIT_SUCCESS);
        } else {
            usage();
            exit(EXIT_FAILURE);
        }
    }

    /* starting the program */

    String key;
    int pipefd[2];
    pid_t pid_1;

    key = getKey(argv[2]);
    pipe(pipefd); /* create pipe for main & process1*/

    pid_1 = fork();
    if (pid_1 == 0) {
        String readPipe;
        initializeString(&readPipe, 100);
        sprintf(readPipe.string, "%d", pipefd[READ]);
        execl("./process-1", readPipe.string, NULL); /* run process 1 binary file */
        printf("Main exec failed.\n");
    } else {
        /*main process*/
        sendToProcess1(argv[1], pipefd, key); /* send plain text */
        exit(EXIT_SUCCESS);
    }
    return EXIT_SUCCESS;
}

void finishProgram(int readPipeProcess3) {
    /* write cipher.txt and finish the program */
    String buffer;
    FILE *file;

    initializeString(&buffer, 100);
    file = fopen(OUT_FILE_MAIN, "w");
    /* read last pipe */
    while (read(readPipeProcess3, buffer.string, 100) != 0) {
        fprintf(file, "%s\n", buffer.string);
        fflush(file);
    }
    fclose(file);
}

void sendToProcess1(char *plainTextPath, int *pipefd, String key) {
    /* send plain text to process-1 block by block with pipe */
    FILE *file;
    int c;

    file = fopen(plainTextPath, "r");

    String plainText;
    initializeString(&plainText, 100);

    write(pipefd[WRITE], key.string, (size_t) key.size);

    /* read plaintext */
    while ((c = fgetc(file)) != EOF) {
        if (c != '\n') {
            char temp[2];
            temp[0] = (char) c;
            temp[1] = '\0';
            concatString(&plainText, temp);
        } else {
            if (plainText.top != -1) {
                concatString(&plainText, "\0");

                /* send block via pipe */
                write(pipefd[WRITE], plainText.string, (size_t) plainText.size);

                clearString(&plainText);
            }
        }
    }
    close(pipefd[WRITE]);
}

/**
 * reads key file and gets key string
 *
 * @param keyFilePath key file absolute path
 * @return string key
 */
String getKey(char *keyFilePath) {
    FILE *file;
    int c;

    file = fopen(keyFilePath, "r");

    String keyString;
    initializeString(&keyString, 100);

    while ((c = fgetc(file)) != EOF) {
        if (c != '\n') {
            char temp[2];
            temp[0] = (char) c;
            temp[1] = '\0';
            concatString(&keyString, temp);
        } else {
            if (keyString.top != -1) {
                concatString(&keyString, "\0");
                break;
                /*clearString(&keyString);*/
            }
        }
    }
    return keyString;
}

void usage() {
    printf("\nUSAGE:       ./a.out <plain_text_file_path> <key_file_path>\n"
           "Example:     ./a.out ./sample/plain.txt ./sample/key.txt\n");
}


/**
 * String initializer
 * @param string String structure
 * @param initialSize size must be positive
 */
void initializeString(String *string, int initialSize) {
    string->string = (char *) malloc(initialSize * sizeof(char));
    string->string[0] = '\0';
    string->size = initialSize;
    string->top = -1;
}

/**
 * concats strings (size auto increased)
 * @param string
 * @param newCharArray
 */
void concatString(String *string, char *newCharArray) {

    /* concats two strings */

    int i;
    for (i = 0; newCharArray[i] != '\0'; i++) {
        if (string->top == string->size - 1) {
            string->size *= 2;
            string->string = (char *) realloc(string->string, string->size * sizeof(char));
        }
        string->top++;
        string->string[string->top] = newCharArray[i];
    }

    if (string->top == string->size - 1) {
        string->size *= 2;
        string->string = (char *) realloc(string->string, string->size * sizeof(char));
    }

    string->string[string->top + 1] = '\0';
}

/**
 * Clear all chars in string but size is same
 * @param string
 */
void clearString(String *string) {

    /* string's chars NULL char "reset string" */

    int i;
    for (i = 0; i <= string->top; i++) {
        string->string[i] = '\0';
    }

    string->top = -1;
}